﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeNameGary.Authority
{
    public class Authority : IAuthority
    {
        public List<Permission> Permissions = new List<Permission>();

        public Dictionary<object, object> Options = new Dictionary<object, object>();

        public bool Can(string activity, object entity)
        {
            Dictionary<object, object> options = new Dictionary<object, object>();
            return this.Can(activity, entity, options);
        }

        public bool Can(string activity, object entity, Dictionary<object, object> options)
        {
            var entityClassName = entity.GetType().Name;
            return this.Can(activity, entityClassName, options);
        }

        public bool Can(string activity, string entityClassName)
        {
            Dictionary<object, object> options = new Dictionary<object, object>();
            return this.Can(activity, entityClassName, options);
        }

        public bool Can(string activity, string entityClassName, object entity)
        {
            Dictionary<object, object> options = new Dictionary<object, object>();
            options.Add("Entity", entity);
            return this.Can(activity, entityClassName, options);
        }

        public bool Can(string activity, string entityClassName, Dictionary<object, object> options)
        {
            bool allow = false;
            this.Options.ToList().ForEach(x => options[x.Key] = x.Value);
            IEnumerable<Permission> permissions = this.Permissions.Where(p => p.EntityClassName == entityClassName && p.Activity == activity);
            foreach(Permission permission in permissions)
            {
                bool result = permission.Evaluate(options);
                if(result == true && permission.Type == Permission.Types.Deny) return false;
                allow = result;
            }
            return allow;
        }

        public void Allow(string activity, string entityClassName, Evaluator evaluator)
        {
            Permission permission = new Permission();
            permission.Type = Permission.Types.Allow;
            permission.Activity = activity;
            permission.EntityClassName = entityClassName;
            permission.Evaluator = evaluator;
            this.Permissions.Add(permission);
        }

    }
}
