﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeNameGary.Authority
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Class, AllowMultiple=true)]
    class AuthorityAttribute : Attribute
    {

    }
}
