﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeNameGary.Authority
{
    public interface IAuthority
    {
        bool Can(string activity, object entity, Dictionary<object, object> options);
        bool Can(string activity, string entityClassName, Dictionary<object, object> options);
        void Allow(string activity, string entityClassName, Evaluator evaluator);
    }
}
