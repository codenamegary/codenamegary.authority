﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeNameGary.Authority
{
    public class Permission
    {

        public enum Types { Allow, Deny };

        public Types Type { get; set; }
        public string Activity { get; set; }
        public string EntityClassName { get; set; }
        public Evaluator Evaluator { get; set; }
        
        public bool Evaluate(Dictionary<object, object> options)
        {
            return this.Evaluator(options);
        }

    }
}
