# Authority

**DISCLAIMER:** Under active development, should not be used in production.

This package is inspired by [CanCan (Ruby)](https://github.com/ryanb/cancan) and [Authority (PHP)](https://packagist.org/packages/machuga/authority).

Authority allows you to define a centralized, reusable set of rules that can be employed across your entire application. Each rule is roughly defined by an activity (Read, Write, Whatever) and an Entity. An Entity may be any class or object that you wish to define access rules for.

Example usage:

    /**
     * Step 1: Instantiate a new Authority instance.
     */
    Authority authority = new Authority();

    /**
     * Step 2: Give Authority some context. Could be the logged in user, a league
     *         or a team object, anything you may want to reference in access
     *         rules later on. Here, we're adding the currently logged in
     *         user to the default options so that we can use it later.
     */
    var currentUser = new User() { FirstName = "Gary", LastName = "Saunders", Id = 1 };
    authority.Options.Add("CurrentUser", currentUser);

    /**
     * Step 3: Some debug / testing output to the console.
     */
    System.Console.WriteLine("Current (Logged in) user is: " + currentUser.FirstName);
    System.Console.WriteLine("User may read or write any user having ID = 1");
            
    /**
     * Step 4: Setup rules. Each of these is basically a callback that gets fired
     *         every time the rule is evaluated. Options, like the CurrentUser
     *         option we set in Step 2 will always be passed through the
     *         options argument.
     */

    // Allow read if there is a user in the options and their Id is 1.
    authority.Allow("read", "User", (options) => {
        currentUser = options["CurrentUser"] as User;
        System.Console.WriteLine("Evaluating user read.");
        var user = options["Entity"] as User;
        return user.Id == 1;
    });

    // Allow write if there is a user in the options and their Id is 1.
    authority.Allow("write", "User", (options) => {
        currentUser = options["CurrentUser"] as User;
        System.Console.WriteLine("Evaluating user write.");
        var user = options["Entity"] as User;
        return user.Id == 1;
    });

    // Allow cornhole if there is a user in the options and their FirstName is Fred.
    authority.Allow("cornhole", "User", (options) => {
        var user = options["Entity"] as User;
        System.Console.WriteLine("Evaluating Cornhole permission for user: " + user.FirstName);
        if (user.FirstName == "Fred") return true;
        return false;
    });

    // Always allow a list of users, regardless of any context / options.
    authority.Allow("list", "User", (options) => {
        return true;
    });

    /**
     * Step 5: Use the rules / auth container!
     */
    System.Console.WriteLine("");

    // Can we read the currently logged in user?
    if (authority.Can("read", "User", currentUser))
    {
        System.Console.WriteLine(string.Format("I can read user {0}!", currentUser.Id));
    }
    else
    {
        System.Console.WriteLine(string.Format("I cannot read user {0}. :( ** Fail horn", currentUser.Id));
    }
    System.Console.WriteLine("");

    // Defining a new user to test with, can we write to their user record?
    var otherUser = new User() { FirstName = "Fred", LastName = "Read", Id = 2 };

    if(authority.Can("write", "User", otherUser))
    {
        System.Console.WriteLine(string.Format("I can write user {0}!", otherUser.Id));
    }
    else
    {
        System.Console.WriteLine(string.Format("I cannot write user {0}. :( ** Fail horn", otherUser.Id));
    }
    System.Console.WriteLine("");

    // Can we cornhole the user?
    if (authority.Can("cornhole", "User", otherUser))
    {
        System.Console.WriteLine(string.Format("I can cornhole user {0}!", otherUser.Id));
    }
    else
    {
        System.Console.WriteLine(string.Format("I cannot cornhole user {0}. :( ** Fail horn", otherUser.Id));
    }
    System.Console.WriteLine("");

    System.Console.ReadLine();
